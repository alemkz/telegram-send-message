/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author developer4
 */
public interface MessageSevice {
    
    public List<Message> getMessages(ArrayList<Notification> notes, String username);
    public String getMessageText(Notification note);
    public  void send(Message Message);
}
