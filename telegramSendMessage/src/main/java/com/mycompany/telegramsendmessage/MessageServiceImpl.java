/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author developer4
 */
public class MessageServiceImpl implements MessageSevice {

    private String sentiment = "";

    public List<Message> getMessages(ArrayList<Notification> notes, String username) {
        List<Message> messages = new ArrayList<>();

        if (!notes.isEmpty()) {
            for (Notification note : notes) {
                Message message = new Message();
                String subject = note.getTopicName();
                String text = "";
                if (subject.contains("$")) {
                    subject = subject.substring(subject.indexOf("$") + 1, subject.length()).trim();
                }
                subject = subject.replace("_", "");
                message.setFrom(username);
                message.setTo(note.getChats().toArray(new String[note.getChats().size()]));
                message.setText(getMessageText(note));
                //message.setTime(note.getTime());
                messages.add(message);
            }
        }
        return messages;

    }

    public String getMessageText(Notification note) {

        // String text = subject + "<hr/>";
        String text = "";
        String datePublished = "";
        String dateCreated = "";
        boolean published = false;
        String titleStyle = "font-size: 15pt;color: #676767; padding: 10px 0px; ";//"color: #3373B7;font-size: 18px;font-family:Arial,sans-serif;padding: 5px 0px;";

        String documentInfoStyle = "min-width: 200px; color: #868686; font-size:10pt;";
        String iconStyle = "width: 14px;height: 14px;display: inline-block;margin: 0px 3px 0 10px;vertical-align:middle;background-size: cover;";
        String ahrefStyle = "padding: 3px 0px;";
        String sentimentStyle = "opacity: 0.7; border-radius: 4px; color: #fff; padding-left: 3px; padding-right: 3px;  "
                + "font-size: 10pt; ";

        String dateStyle = "margin: 3px 0px;";
        String textStyle = "color: #000;font-size: 10pt;";

        switch (note.getSentiment()) {
            case "NEGATIVE":
                sentimentStyle += "background: #db4a51;";
                break;
            case "POSITIVE":
                sentimentStyle += "background: #66b737;";
                break;
            case "NEUTRAL":
                sentimentStyle += "background: #929292;";
                break;
            case "UNDEFINED":
                sentimentStyle += "background: #83B5E8;";
                break;
            default:
                sentimentStyle += "background: #83B5E8;";
                break;
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        if (note.getDatePublished() != null && (note.getDatePublished().getHours() != 0 || note.getDatePublished().getMinutes() != 0)) {
            published = true;
            datePublished = dateFormatter.format(note.getDatePublished());
        }
        if (note.getDateCreated().getHours() == 0 && note.getDateCreated().getMinutes() == 0) {
            dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        }
        dateCreated = dateFormatter.format(note.getDateCreated());
        
    
        text += "<div style='margin: 0px 30px 0px 30px; padding-bottom: 30px; font-family: Arial,Helvetica,Verdana,sans-serif; font-weight: normal;'>"
                + "<div style='" + titleStyle + "'><a href='" + note.getUrl() + "' target='_blank' style='color:#3373b7;'>" + note.getTitle() + "</a></div>";
        text += "<div style='" + documentInfoStyle + "'>"
                + "<div><span style= '" + iconStyle + "background-image: url(\"https://cabinet.alem.kz/resources/imgs/source-icon.png\")" + "'>"
                + "</span><span>"+"\n" +"Источник: <a href=\"http://" + note.getDomain() + "\" target='_blank' style ='"
                + ahrefStyle + "'>" + note.getDomain() + "</a></span>";

        if (note.getScope().contains("SENTIMENT")) {
            text += "<span style= '" + iconStyle + "background-image: url(\"https://cabinet.alem.kz/resources/imgs/sentiment-icon.png\")" + "'>"
                    + "</span><span>"+"\n" +"Тональность: " + "</span><span style= '" + sentimentStyle + "'> " + getSentimentInRus(note.getSentiment()) + " </span>";
        }
        text += "</div>";

        if (published == true) {
            text += "<div style='" + dateStyle + "'>"
                    + "<span style='" + iconStyle + "background-image: url(\"https://cabinet.alem.kz/resources/imgs/published-icon.png\")" + "'>"
                    + "</span><span style=/'font-weight: bold;/'>"+"\n" +"Дата публикации: </span><span style='font-weight: bold;'>" + datePublished + "</span></span>"
                    + "<span style='" + iconStyle + "background-image: url(\"https://cabinet.alem.kz/resources/imgs/crawled-icon.png\")" + "'>"
                    + "</span><span style=/'font-weight: bold;/'>"+"\n" +"Дата сбора: </span><span style='font-weight: bold;'>" + dateCreated + "\n" +"</span></span></div>";
        } else {
            text += "<div style='" + dateStyle + "'>"
                    + "<span style='" + iconStyle + "background-image: url(\"https://cabinet.alem.kz/resources/imgs/crawled-icon.png\")" + "'>"
                    + "</span><span>"+"\n" +"Дата сбора: </span><span style='font-weight: bold;'>" + dateCreated + "\n" +"</span></span></div>";
        }
        text += "</div>";
        text += "<div ><p><span style= '" + textStyle + "'>";
        for (String hl : note.getHighlight()) {
            int ind = hl.lastIndexOf(",");
            if (!hl.contains("_big_") && !hl.contains("_small_")) {
                if (ind > -1) {
                    text += " " + hl.substring(0, ind);
                } else {
                    text += " " + hl;
                }
            }
        }
        text += "</span></p></div></div>";
        return text.replaceAll("\\<.*?\\>", "");
   
    }

    public void send(Message Message) {

       

        String apiToken = "564309724:AAH0LAcF0WwPQLp6iPn1zDRPPnJTppRbWoA";

        String[] chats = Message.getTo(); //"405471847";

        String text = Message.getText();
        for (int i = 0; i < chats.length; i++) {
            try {
                String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
                urlString = String.format(urlString, apiToken, chats[i], URLEncoder.encode(text, "UTF-8"));
                URL url = new URL(urlString);
                URLConnection conn = url.openConnection();
                StringBuilder sb = new StringBuilder();
                InputStream is = new BufferedInputStream(conn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String inputLine = "";
                while ((inputLine = br.readLine()) != null) {
                    sb.append(inputLine);
                }
                String response = sb.toString();
             }  catch (Exception e) {
                 e.printStackTrace();
            }
        }
    }
    public String getSentimentInRus(String sentimentEng) {
        switch (sentimentEng) {
            case "NEGATIVE":
                sentiment = "Негативная";
                break;
            case "POSITIVE":
                sentiment = "Позитивная";
                break;
            case "NEUTRAL":
                sentiment = "Нейтральная";
                break;
            case "UNDEFINED":
                sentiment = "Не определена";
                break;
        }
        return sentiment;
    }

}
