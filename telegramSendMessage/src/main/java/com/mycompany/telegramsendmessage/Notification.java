/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;

/**
 *
 * @author developer4
 */

import java.util.Date;
import java.util.List;

public class Notification {

    private List<String>  chats;
    private List<String> highlight;
    private String url;
    private String title;
    private int time;
    private String topicName;
    private String topicId;
    private String domain;
    private Date datePublished;
    private Date dateCreated;
    private String mediaType;
    private String sentiment;
    private String scope;
   
    public List<String> getChats() {
        return chats;
    } 
    
    public void setChats(List<String> chats) {
        this.chats = chats;
    }
    
    /**
     * @return the highlight
     */
    public List<String> getHighlight() {
        return highlight;
    }

    /**
     * @param highlight the highlight to set
     */
    public void setHighlight(List<String> highlight) {
        this.highlight = highlight;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the time
     */
    public int getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * @return the topicName
     */
    public String getTopicName() {
        return topicName;
    }

    /**
     * @param topicName the topicName to set
     */
    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    /**
     * @return the topicId
     */
    public String getTopicId() {
        return topicId;
    }

    /**
     * @param topicId the topicId to set
     */
    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    /**
     * @return the urlName
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param urlName the urlName to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the sentiment
     */
    public String getSentiment() {
        return sentiment;
    }

    /**
     * @param sentiment the sentiment to set
     */
    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }
    /**
     * @return the datePablished
     */
    public Date getDatePublished() {
        return datePublished;
    }

    /**
     * @param datePablished the datePablished to set
     */
    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the mediaType
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * @param mediaType the mediaType to set
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
    /**
     * @param scope the scope to set
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getScope() {
         return scope;
    }

}

