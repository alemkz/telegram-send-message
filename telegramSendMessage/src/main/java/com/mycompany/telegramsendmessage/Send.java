/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;
/**
 *
 * @author developer4
 */
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeoutException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@ImportResource({"classpath:context.xml"})
public class Send {

    private MessageSevice messageService;
    private String from;
    private static final int SEND_TIME = 1 * 60 * 1000; //1 minutes

    private final static String QUEUE_NAME = "outputQueue";

    public static void main(String[] argv) throws Exception {
        new Send().test();
    }
    
     public Send() throws IOException {
        this.setProperties();
    }
      private void setProperties() throws IOException {
        SendConfig appConfig = new SendConfig();
        messageService = appConfig.getMessageService();
        from = "";
    }
    public void test() throws ParseException {
        RabbitClient rc;
        try {
            rc = new RabbitClient();
            rc.consume();
            while (true) {
                try {
                    Thread.sleep(SEND_TIME);
                } catch (InterruptedException ex) {
                    // log.error("Exception: ", ex);
                }
                Message m = new Message();
                // messageService.send(m);
                List<Notification> notifications = rc.getNotifications();
                List<Message> messages = messageService.getMessages((ArrayList<Notification>) notifications, from);
                for (Message message : messages) {
                    List<String> to = new ArrayList<>();
                    for (String mail : message.getTo()) {
                        if (!mail.equals(null)) {
                            to.add(mail);
                        }
                    }
                    // log.info("Adding to emailsTo " + to);
                    message.setTo(to.toArray(new String[to.size()]));
                    if (message != null && message.getTo().length > 0) {
                        messageService.send(message);
                    }
                }

            }
        } catch (IOException | TimeoutException ex) {
            //log.error("Exception: ", ex);
        }
    }

}
