/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;

/**
 *
 * @author developer4
 */
public class Message {
    
    private String from;
    private String[] to;
    private String text;
    private int time;
    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String[] getTo() {
        return to;
    }
    public void setTo(String[] to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }    
    public int getTime() {
        return time;
    }
    public void setTime(int time) {
        this.time = time;
    }
}
