/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;

import java.io.IOException;
import java.util.Properties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

/**
 *
 * @author developer4
 */
public class SendConfig {
  
    private final String inputQueueName;
    private final String host;
    private final String user;
    private final String password;
    private final MessageSevice messageService;
      //private final MessageService messageService;
    
    public SendConfig() throws IOException{
        Resource resource = new ClassPathResource("/config.properties");
        Properties props = PropertiesLoaderUtils.loadProperties(resource);
        //RabbitMQ configs
        inputQueueName = props.getProperty("input.queue");
        host = props.getProperty("rabbitmq.host");
        user = props.getProperty("rabbitmq.user");
        password = props.getProperty("rabbitmq.password");
        messageService =  new MessageServiceImpl();
    }

    public String getHost() {
        return host;
    }

    public String getInputQueueName() {
        return inputQueueName;
    }

    public String getPassword() {
        return password;
    }

    public String getUser() {
        return user;
    }

    public MessageSevice getMessageService() {
        return messageService;
    }
    
    
    
    
    
    
}
