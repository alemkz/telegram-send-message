/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegramsendmessage;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.impl.DefaultExceptionHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author developer4
 */
public class RabbitClient {

    private final Channel channel;
    private final Connection connection;
    private final String inputQueueName;
    private List<Notification> notifications = new ArrayList<>();
    private final Gson gson = new Gson();
    Date startDate;
    private final int UPDATE_TIME_SAVING = 24 * 60 * 60 * 1000; //for KaspiBank
    private HashMap<String, ArrayList<String>> titleDomains; //for KaspiBank

    public RabbitClient() throws IOException, TimeoutException {
        SendConfig sendConfig = new SendConfig();
        inputQueueName = sendConfig.getInputQueueName();   //"email-send-q";
        String host =  sendConfig.getHost(); //"localhost"; //"127.0.0.1:15672" ; //appConfig.getHost();
        String user =  sendConfig.getUser();//"radmin";//appConfig.getUser();
        String password = sendConfig.getPassword(); //"radmin"; // appConfig.getPassword();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPassword(password);
        factory.setUsername(user);
        factory.setConnectionTimeout(30000);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setTopologyRecoveryEnabled(true);
        factory.setNetworkRecoveryInterval(10000);
        factory.setExceptionHandler(new DefaultExceptionHandler());
        factory.setRequestedHeartbeat(360);
        connection = factory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(inputQueueName, true, false, false, null);
        channel.basicQos(10);
        channel.addShutdownListener(null);
        startDate = new Date();
        titleDomains = new HashMap<>();
    }
    public List<Notification> getNotifications() {
        List<Notification> thirtyNotifications = new ArrayList<>();
        int size = notifications.size();
        if (size > 0) {
            if (size >= 30) {
                size = 30;
            }
            for (int i = 0; i < size; i++) {
                thirtyNotifications.add(notifications.get(i));
                //   log.info("Notification in client: " + thirtyNotifications.get(i).getEmails());
            }
            notifications.removeAll(thirtyNotifications);
        }
        return thirtyNotifications;
    }

    public void consume() throws IOException {
        channel.basicConsume(inputQueueName, false,
                new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag,
                    Envelope envelope,
                    AMQP.BasicProperties properties,
                    byte[] body) {
                try {
                    long deliveryTag = envelope.getDeliveryTag();
                    String rawBody = new String(body, "UTF-8");
                    Notification note = gson.fromJson(rawBody, Notification.class);
                    boolean copy = false;
                    if (!titleDomains.isEmpty() && titleDomains.containsKey(note.getDomain().trim() + " " + note.getTitle().trim())) {
                        for (String email : titleDomains.get(note.getDomain().trim() + " " + note.getTitle().trim())) {
                            for (String noteEmail : note.getChats()) {
                                if (email.equals(noteEmail)) {
                                    copy = true;
                                    //   log.info("duplicated domain and titles "+ note.getDomain().trim()+" "+note.getTitle().trim());
                                    break;
                                }
                            }
                        }
                    }
                    if (copy == false) {
                        if (titleDomains.isEmpty() || !titleDomains.containsKey(note.getDomain().trim() + " " + note.getTitle().trim())) {
                            ArrayList<String> chats = new ArrayList<>(note.getChats());
                            titleDomains.put(note.getDomain() + " " + note.getTitle().trim(), chats);

                        } else {
                            for (String email : note.getChats()) {
                                titleDomains.get(note.getDomain() + " " + note.getTitle().trim()).add(email);
                            }
                        }
                        notifications.add(note);
                    }
                    if (UPDATE_TIME_SAVING < System.currentTimeMillis() - startDate.getTime()) {
//                        log.info("URLS LAST HASH CODE " + titleDomains.hashCode());
//                       for(String td: titleDomains.keySet() ){
//                           log.info("ALL domain and titles "+ td);
//                       }
                        startDate = new Date();
                        titleDomains = new HashMap<>();
                    }
                    //log.info("deliveryTag " + deliveryTag);
                    channel.basicAck(deliveryTag, false);
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    //   log.error("Exception: " + ex);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    ///  log.error("Exception: " + ex);
                }
            }
        });
    }
}
